import customer.Customer;
import database.DataAccessObject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class CustomerImporterMain {

    private static final DataAccessObject dataAccessObject = new DataAccessObject("localhost", 3306, "customer-importer", "root", "");

    public static void main(String[] args) throws IOException {
        Reader in = new FileReader("kundendaten.csv");
        var records = CSVFormat.newFormat(';').withFirstRecordAsHeader().parse(in);
        for (CSVRecord record : records) {
            Customer customer = new Customer(Integer.parseInt(record.get(0)), record.get(1), record.get(2), Integer.parseInt(record.get(3)), record.get(4), record.get(5), record.get(6));
            dataAccessObject.importCustomer(customer);
        }
    }
}
