package database;

import customer.Customer;

import java.sql.*;

public class DataAccessObject {

    private final String user;

    private final String password;

    private final String databaseUrl;

    public DataAccessObject(String host, int port, String database, String user, String password) {
        this.user = user;
        this.password = password;

        this.databaseUrl = "jdbc:mariadb://" + host + ":" + port + "/" + database;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    public void importCustomer(Customer customer) {
        Connection c = connect();
        PreparedStatement ps;
        try {
            ps = c.prepareStatement("INSERT INTO `customer`(`Kundennummer`, `Vorname`, `Nachname`, `Plz`, `Ort`, `StraßeHausnummer`, `E-Mail`) VALUES (?,?,?,?,?,?,?)");
            ps.setInt(1, customer.getKundennummer());
            ps.setString(2, customer.getVorname());
            ps.setString(3, customer.getNachname());
            ps.setInt(4, customer.getPlz());
            ps.setString(5, customer.getOrt());
            ps.setString(6, customer.getStraßeHausnummer());
            ps.setString(7, customer.geteMail());
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(c);
        }
    }

    private Connection connect() {
        try {
            return DriverManager.getConnection(this.databaseUrl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }
}
