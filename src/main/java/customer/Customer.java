package customer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Customer {

    int kundennummer;
    String vorname;
    String nachname;
    int plz;
    String ort;
    String straßeHausnummer;
    String eMail;

    public Customer(int kundennummer, String vorname, String nachname, int plz, String ort, String straßeHausnummer, String eMail) {
        this.kundennummer = kundennummer;
        this.vorname = vorname;
        this.nachname = nachname;
        this.plz = plz;
        this.ort = ort;
        this.straßeHausnummer = straßeHausnummer;
        this.eMail = eMail;
    }

    public int getKundennummer() {
        return kundennummer;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public int getPlz() {
        return plz;
    }

    public String getOrt() {
        return ort;
    }

    public String getStraßeHausnummer() {
        return straßeHausnummer;
    }

    public String geteMail() {
        return eMail;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "kundennummer=" + kundennummer +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", plz=" + plz +
                ", ort='" + ort + '\'' +
                ", straßeHausnummer='" + straßeHausnummer + '\'' +
                ", eMail='" + eMail + '\'' +
                '}';
    }
}
